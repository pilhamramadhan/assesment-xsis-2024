package xa.assement.assement_Rental_Mobil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssementRentalMobilApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssementRentalMobilApplication.class, args);
	}

}
