package xa.assement.assement_Rental_Mobil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.assement.assement_Rental_Mobil.models.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer, Long> {

    // search
    @Query(value = "select * FROM customer WHERE lower(nama) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<Customer> SearchCus(String keyword);

    // validation pakek nama
    @Query (value = "select * FROM customer WHERE lower(nama) = lower(:nama)", nativeQuery = true)
    Optional<Customer> searchByNama (String nama);

}
