package xa.assement.assement_Rental_Mobil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.assement.assement_Rental_Mobil.models.RentalMobil;

import java.util.List;

public interface RentalMobilRepo extends JpaRepository<RentalMobil, Long> {
    @Query(value = " select rental_mobil.* FROM rental_mobil \n" +
            "join mobil on rental_mobil.mobil = mobil.id\n" +
            "join customer on rental_mobil.customer = customer.id WHERE lower(customer.nama) LIKE lower(concat('%',:keyword,'%')) or lower(mobil.nama_mobil) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<RentalMobil> Search(String keyword);

}
