package xa.assement.assement_Rental_Mobil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.assement.assement_Rental_Mobil.models.JenisMobil;
import xa.assement.assement_Rental_Mobil.models.Mobil;

import java.util.List;
import java.util.Optional;

public interface MobilRepo extends JpaRepository<Mobil, Long> {
    @Query(value = "select * FROM mobil WHERE lower(nama_mobil) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<Mobil> Search(String keyword);

    // validasi nama
    @Query (value = "select * FROM mobil WHERE lower(nama_mobil) = lower(:mobil)", nativeQuery = true)
    Optional<Mobil> searchBymobil (String mobil);

}
