package xa.assement.assement_Rental_Mobil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.assement.assement_Rental_Mobil.models.JenisMobil;

import java.util.List;
import java.util.Optional;

public interface JenisMobilRepo extends JpaRepository<JenisMobil, Long>{
    @Query(value = "select * FROM jenis_mobil WHERE lower(jenis_mobil.jenis) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<JenisMobil> Search(String keyword);

    // ini buat validasi
    @Query (value = "select * FROM jenis_mobil WHERE lower(jenis) = lower(:jenis)", nativeQuery = true)
    Optional<JenisMobil> searchByJenis (String jenis);
}
