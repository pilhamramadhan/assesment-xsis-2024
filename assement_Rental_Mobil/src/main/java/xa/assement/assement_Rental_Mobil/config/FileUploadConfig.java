package xa.assement.assement_Rental_Mobil.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfig implements WebMvcConfigurer {

    String mypath ="file:///D:/Assement_Xsis/assement_FrontEnd_Rental_Mobil/src/main/resources/image/";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler("/file/**").addResourceLocations(mypath);
    }

}
