package xa.assement.assement_Rental_Mobil.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xa.assement.assement_Rental_Mobil.models.Mobil;
import xa.assement.assement_Rental_Mobil.models.RentalMobil;
import xa.assement.assement_Rental_Mobil.repositories.RentalMobilRepo;

import java.util.List;

@Service
public class RentalMobilService {

    @Autowired
    RentalMobilRepo rentalMobilRepo;

    public List<RentalMobil> getAllRentalMobil(){
        return this.rentalMobilRepo.findAll();
    }

    public RentalMobil getRentalMobilById(Long id){
        return this.rentalMobilRepo.findById(id).orElse(null);
    }

    public void saveRentalMobil(RentalMobil customer){
        this.rentalMobilRepo.save(customer);
    }

    public void deleteRentalMobil(Long id){
        this.rentalMobilRepo.deleteById(id);
    }

    public List<RentalMobil> searchRental(String keyword) {
        return this.rentalMobilRepo.Search(keyword);
    }
}
