package xa.assement.assement_Rental_Mobil.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.assement.assement_Rental_Mobil.models.JenisMobil;
import xa.assement.assement_Rental_Mobil.models.Mobil;
import xa.assement.assement_Rental_Mobil.repositories.MobilRepo;

import java.util.List;
import java.util.Optional;

@Service
public class MobilService {

    @Autowired
    MobilRepo mobilRepo;

    public List<Mobil> getAllMobil(){
        return this.mobilRepo.findAll();
    }

    public Mobil getMobilById(Long id){
        return this.mobilRepo.findById(id).orElse(null);
    }

    public Optional<Mobil> getIdMobil(Long id){
        return this.mobilRepo.findById(id);
    }

    public void saveMobil(Mobil customer){
        this.mobilRepo.save(customer);
    }

    public void deleteMobil(Long id){
        this.mobilRepo.deleteById(id);
    }

    public List<Mobil> searchMobil(String keyword) {
        return this.mobilRepo.Search(keyword);
    }

    public Optional<Mobil> searchByMobil(String mobil){
        return this.mobilRepo.searchBymobil(mobil);
    }

    public Mobil objectMapper(String mobilData) {
        try {
            Mobil mobil = new Mobil();
            ObjectMapper objectMapper = new ObjectMapper();
            mobil = objectMapper.readValue(mobilData, Mobil.class);
            return mobil;
        }catch (Exception e){
            return null;
        }
    }
}
