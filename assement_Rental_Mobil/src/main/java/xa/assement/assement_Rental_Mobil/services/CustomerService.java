package xa.assement.assement_Rental_Mobil.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.assement.assement_Rental_Mobil.models.Customer;
import xa.assement.assement_Rental_Mobil.repositories.CustomerRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    CustomerRepo customerRepo;

    public List<Customer> getAllCustomer(){
        return this.customerRepo.findAll();
    }

//    public Customer getCustomerById(Long id){
//        return this.customerRepo.findById(id).orElse(null);
//    }

    public Optional<Customer> getByIdCustomer (Long id){
        return this.customerRepo.findById(id);
    }

    public void saveCustomer(Customer customer){
        this.customerRepo.save(customer);
    }

    public void deleteCustomer(Long id){
        this.customerRepo.deleteById(id);
    }

    public List<Customer> searchCustomer(String keyword) {
        return this.customerRepo.SearchCus(keyword);
    }

    // validasi dengan nama
    public Optional<Customer> searchByNama(String nama){
        return this.customerRepo.searchByNama(nama);
    }

    // Gambar
    public byte[] readFile(Long id){
        Customer customer =this.customerRepo.findById(id).orElse(null);
        if (customer != null){
            return customer.getFileContent();
        } else {
            return null;
        }
    }

}
