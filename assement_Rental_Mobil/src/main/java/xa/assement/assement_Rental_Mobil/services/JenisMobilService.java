package xa.assement.assement_Rental_Mobil.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.assement.assement_Rental_Mobil.models.JenisMobil;
import xa.assement.assement_Rental_Mobil.repositories.JenisMobilRepo;

import java.util.List;
import java.util.Optional;

@Service
public class JenisMobilService {

    @Autowired
    JenisMobilRepo jenisMobilRepo;

    public List<JenisMobil> getAllJenisMobil(){
        return this.jenisMobilRepo.findAll();
    }

    public JenisMobil getJenisMobilById(Long id){
        return this.jenisMobilRepo.findById(id).orElse(null);
    }

    public Optional<JenisMobil> getIdJenisMobil(Long id){
        return this.jenisMobilRepo.findById(id);
    }

    public void saveJenisMobil(JenisMobil customer){
        this.jenisMobilRepo.save(customer);
    }

    public void deleteJenisMobil(Long id){
        this.jenisMobilRepo.deleteById(id);
    }

//    public List<JenisMobil> searchJenis(String keyword) {
//        return this.jenisMobilRepo.Search(keyword);
//    }

    // validasi Nama
    public Optional<JenisMobil> searchByJenis(String jenis){
        return this.jenisMobilRepo.searchByJenis(jenis);
    }

}
