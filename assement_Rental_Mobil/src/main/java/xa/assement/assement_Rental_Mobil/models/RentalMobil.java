package xa.assement.assement_Rental_Mobil.models;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "rental_mobil")
public class    RentalMobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "customer", insertable = false, updatable = false)
    public Customer customer;

    @Column(name = "customer")
    private Long customerId;

    @ManyToOne
    @JoinColumn(name = "mobil", insertable = false,updatable = false)
    public Mobil mobil;

    @Column(name = "mobil")
    private Long mobilId;

    @Column(name = "tgl_sewa")
    private Date TglSewa;

    @Column(name = "tgl_balik_sewa")
    private Date TglBalikSewa;

    @Column(name = "price")
    private Integer Price;

    @Column(name = "hari")
    private Integer Hari;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Mobil getMobil() {
        return mobil;
    }

    public void setMobil(Mobil mobil) {
        this.mobil = mobil;
    }

    public Long getMobilId() {
        return mobilId;
    }

    public void setMobilId(Long mobilId) {
        this.mobilId = mobilId;
    }

    public Date getTglSewa() {
        return TglSewa;
    }

    public void setTglSewa(Date tglSewa) {
        TglSewa = tglSewa;
    }

    public Date getTglBalikSewa() {
        return TglBalikSewa;
    }

    public void setTglBalikSewa(Date tglBalikSewa) {
        TglBalikSewa = tglBalikSewa;
    }

    public Integer getPrice() {
        return Price;
    }

    public void setPrice(Integer price) {
        Price = price;
    }

    public Integer getHari() {
        return Hari;
    }

    public void setHari(Integer hari) {
        Hari = hari;
    }
}
