package xa.assement.assement_Rental_Mobil.models;

import jakarta.persistence.*;


@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "nama")
    private String Nama;

    @Column(name = "jenis_kelamin")
    private String JenisKelamin;

    @Column(name = "alamat")
    private String Alamat;

    @Column(name = "file_path", length = 200)
    private String FilePath;

    @Lob
    @Column(name = "file_content")
    private byte[] FileContent;

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public byte[] getFileContent() {
        return FileContent;
    }

    public void setFileContent(byte[] fileContent) {
        FileContent = fileContent;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getJenisKelamin() {
        return JenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        JenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }

}
