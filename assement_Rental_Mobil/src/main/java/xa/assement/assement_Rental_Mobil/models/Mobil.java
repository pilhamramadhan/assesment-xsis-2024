package xa.assement.assement_Rental_Mobil.models;

import jakarta.persistence.*;

@Entity
@Table(name = "mobil")
public class Mobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "jenis", insertable = false, updatable = false)
    public JenisMobil jenisMobil;

    @Column(name = "jenis")
    private Long jenisMobilId;

    @Column(name = "nama_mobil")
    private String Mobil;

    @Column(name = "bahan_bakar")
    private String BahanBakar;

    @Column(name = "image_file")
    private byte[] image_file;

    @Column(name = "image_path")
    private String image_path;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public JenisMobil getJenisMobil() {
        return jenisMobil;
    }

    public void setJenisMobil(JenisMobil jenisMobil) {
        this.jenisMobil = jenisMobil;
    }

    public Long getJenisMobilId() {
        return jenisMobilId;
    }

    public void setJenisMobilId(Long jenisMobilId) {
        this.jenisMobilId = jenisMobilId;
    }

    public String getMobil() {
        return Mobil;
    }

    public void setMobil(String mobil) {
        Mobil = mobil;
    }

    public String getBahanBakar() {
        return BahanBakar;
    }

    public void setBahanBakar(String bahanBakar) {
        BahanBakar = bahanBakar;
    }

    public byte[] getImage_file() {
        return image_file;
    }

    public void setImage_file(byte[] image_file) {
        this.image_file = image_file;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
