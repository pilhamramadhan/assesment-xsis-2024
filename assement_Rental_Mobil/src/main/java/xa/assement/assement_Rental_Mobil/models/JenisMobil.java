package xa.assement.assement_Rental_Mobil.models;

import jakarta.persistence.*;

@Entity
@Table(name = "jenis_mobil")
public class JenisMobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "jenis")
    private String Jenis;

    @Column(name = "description")
    private String Description;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getJenis() {
        return Jenis;
    }

    public void setJenis(String jenis) {
        Jenis = jenis;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
