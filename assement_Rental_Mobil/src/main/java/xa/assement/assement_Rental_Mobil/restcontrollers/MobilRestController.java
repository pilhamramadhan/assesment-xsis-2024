package xa.assement.assement_Rental_Mobil.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xa.assement.assement_Rental_Mobil.models.Mobil;
import xa.assement.assement_Rental_Mobil.services.MobilService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class MobilRestController {

    @Autowired
    MobilService mobilService;

    private Map<String, String> result = new HashMap<>();

    private static String UPLOAD_F = "D:/Assement_Xsis/assement_FrontEnd_Rental_Mobil/src/main/resources/image/";


    @GetMapping("mobil")
    public ResponseEntity<List<Mobil>> getAllMobil(){
        try {
            List<Mobil> mobil = this.mobilService.getAllMobil();
            return new ResponseEntity<List<Mobil>>(mobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("mobil/{id}")
    public ResponseEntity<?> getMobilById(@PathVariable("id") Long id){
        try {
            Mobil mobil = this.mobilService.getMobilById(id);
            return new ResponseEntity <>(mobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("mobil")
    public ResponseEntity<Object> saveMobil(@RequestBody Mobil mobil){
        try {
            Optional<Mobil> data = this.mobilService.searchByMobil(mobil.getMobil().trim());
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                this.mobilService.saveMobil(mobil);
                result.put("status", "belum");
                return new ResponseEntity <>(result, HttpStatus.OK);
            }
        } catch (Exception e){
            result.put("Message",e.getMessage());
            result.put("Data",null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "mobil/addImage", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> saveAddImage (@RequestPart("mobil") String dataMobil, @RequestPart("file") MultipartFile file){
        try {
            Mobil mobil = mobilService.objectMapper(dataMobil);
            Optional<Mobil> data = this.mobilService.searchByMobil(mobil.getMobil().trim());
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                byte[] itemByte = file.getBytes();
                mobil.setImage_file(itemByte);
                mobil.setImage_path("/file/" + file.getOriginalFilename());
                this.mobilService.saveMobil(mobil);
                result.put("status", "belum");
                Path path = Paths.get(UPLOAD_F + file.getOriginalFilename());
                Files.write(path, itemByte);
                return new ResponseEntity <>(result, HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("mobil/{id}")
    public ResponseEntity<?> editMobil(@RequestBody Mobil mobil, @PathVariable("id") Long id){
        try {
            mobil.setId(id);
            this.mobilService.saveMobil(mobil);
            Map<String, Object> result = new HashMap<>();
            result.put("Message", "Edit Data Success");
            result.put("Status", "200");
            result.put("Data", mobil);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e){
            result.put("Message",e.getMessage());
            result.put("Data",null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "mobil/addImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> saveImage (@RequestParam("id") Long id, @RequestParam("file")MultipartFile file){
        try {
            Optional<Mobil> mobil = this.mobilService.getIdMobil(id);
            if (mobil.isPresent()){
                byte[] itemByte = file.getBytes();

                mobil.get().setImage_file(itemByte);
                mobil.get().setImage_path("/file/" + file.getOriginalFilename());

                this.mobilService.saveMobil(mobil.get());

                Path path = Paths.get(UPLOAD_F + file.getOriginalFilename());
                Files.write(path, itemByte);
                return new ResponseEntity<>("Disimpan", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("gagal", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("mobil/{id}")
    public ResponseEntity<?> deleteMobil(@PathVariable("id") Long id){
        try {
            Mobil mobil = this.mobilService.getMobilById(id);
            if (mobil != null){
                this.mobilService.deleteMobil(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found");
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("mobil={keyword}")
    public ResponseEntity<List<Mobil>> searchMobil(@PathVariable("keyword") String keyword)
    {
        try {
            List<Mobil> mobil = this.mobilService.searchMobil(keyword);
            return new ResponseEntity<>(mobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
