package xa.assement.assement_Rental_Mobil.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xa.assement.assement_Rental_Mobil.models.Customer;
import xa.assement.assement_Rental_Mobil.repositories.CustomerRepo;
import xa.assement.assement_Rental_Mobil.services.CustomerService;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService ;

    @Autowired
    private CustomerRepo customerRepo;

    private Map<String, Object> result;

    private static String UPLOAD_F = "D:/Assement_Xsis/assement_Rental_Mobil/src/main/resources/image/";

    @GetMapping("customer")
    public ResponseEntity<List<Customer>> getAllCustomer(){
        try {
            List<Customer> customer = this.customerService.getAllCustomer();
            return new ResponseEntity<List<Customer>>(customer, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("customer={keyword}")
    public ResponseEntity<List<Customer>> searchCustomer(@PathVariable("keyword") String keyword)
    {
        try {
            List<Customer> customer = this.customerService.searchCustomer(keyword);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("customer/mapped")
    public ResponseEntity<Object> getAllPage(@RequestParam(defaultValue = "0") Integer halaman,
                                             @RequestParam(defaultValue = "5") Integer ukuranHal){
        try {
            Pageable pagingSort = PageRequest.of(halaman, ukuranHal);
            Page<Customer> pageTuts;
            pageTuts = this.customerRepo.findAll(pagingSort);
            List<Customer> customer = pageTuts.getContent();

            result = new HashMap<>();
            result.put("customerAPI", customer);
            result.put("halamanSekarang", pageTuts.getNumber());
            result.put("totalData", pageTuts.getTotalElements());
            result.put("totalHalaman", pageTuts.getTotalPages());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @GetMapping("customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Long id){
        try {
            Optional<Customer> customer = this.customerService.getByIdCustomer(id);
            return new ResponseEntity <>(customer, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


//    @PostMapping("customer")
//    public ResponseEntity<Object> saveCustomer(@RequestBody Customer customer, @RequestParam("filepath")MultipartFile file){
//        try {
//            Optional<Customer> data = this.customerService.searchByNama(customer.getNama().trim());
//            result = new HashMap<>();
//            if (data.isPresent()){
//                result.put("status", "ada");
//                return new ResponseEntity<>(result, HttpStatus.OK);
//            } else if (file.getOriginalFilename() != ""){
//
//                Path path = Paths.get(UPLOAD_F + file.getOriginalFilename());
//
//                byte[] fileContent = file.getBytes();
//
//                customer.setFilePath("/file/" + file.getOriginalFilename());
//                customer.setFileContent(fileContent);
//
//                this.customerService.saveCustomer(customer);
//
//                Files.write(path, fileContent);
//                result.put("status", "belum");
//                result.put("data", customer);
//                return new ResponseEntity<>(result, HttpStatus.OK);
//            } else {
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//            }
//        } catch (Exception e){
//            result.put("pesan", e.getMessage());
//            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @PostMapping("customer")
    public ResponseEntity<Object> saveCustomer(@RequestBody Customer customer){
        try {
            Optional<Customer> data = this.customerService.searchByNama(customer.getNama().trim());
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                this.customerService.saveCustomer(customer);
                result.put("status", "belum");
                result.put("data", customer);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        } catch (Exception e){
            result.put("pesan", e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("customer/{id}")
    public ResponseEntity<?> editJenisMobil (@RequestBody Customer customer, @PathVariable("id") Long id){
        try {
            customer.setId(id);
            Optional<Customer> IdCus = this.customerService.getByIdCustomer(id);
            result = new HashMap<>();
            if (IdCus.isPresent()){
                Customer dataCustomer = IdCus.get();
                if (customer.getNama().equals(dataCustomer.getNama())){ // cek nama sama dengan awal
                    dataCustomer.setNama(customer.getNama());
                    this.customerService.saveCustomer(customer);
                    result.put("Pesan", "Berhasil");
                    result.put("data", customer);
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else { // jika nama di ganti
                    Optional<Customer> customerNamaSama = this.customerService.searchByNama(customer.getNama().trim());
                    if (customerNamaSama.isPresent()){ // jika nama ada yang sama
                        result.put("status", "ada");
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    } else { // jika nama tidak ada yang sama
                        dataCustomer.setNama(customer.getNama());
                        this.customerService.saveCustomer(customer);
                        result.put("Pesan", "Berhasil");
                        result.put("data", customer);
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    }
                }
            } else {
                result.put("Pesan", "ID tidak ditemukan.");
                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            result.put("pesan", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @DeleteMapping("customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id){
        try {
            Optional<Customer> customer = this.customerService.getByIdCustomer(id);
            if (customer != null){
                this.customerService.deleteCustomer(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found");
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



}
