package xa.assement.assement_Rental_Mobil.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.assement.assement_Rental_Mobil.models.RentalMobil;
import xa.assement.assement_Rental_Mobil.services.RentalMobilService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class RentalMobilRestController {
    @Autowired
    RentalMobilService rentalMobilService;

    @GetMapping("rentalmobil")
    public ResponseEntity<List<RentalMobil>> getAllRentalMobil(){
        try {
            List<RentalMobil> rentalMobil = this.rentalMobilService.getAllRentalMobil();
            return new ResponseEntity<List<RentalMobil>>(rentalMobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("rentalmobil/{id}")
    public ResponseEntity<?> getRentalMobilById(@PathVariable("id") Long id){
        try {
            RentalMobil rentalMobil = this.rentalMobilService.getRentalMobilById(id);
            return new ResponseEntity <>(rentalMobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("rentalmobil")
    public ResponseEntity<RentalMobil> saveRentalMobil(@RequestBody RentalMobil rentalMobil){
        long totalHari =rentalMobil.getTglBalikSewa().getTime() - rentalMobil.getTglSewa().getTime();
        long bedaHari =totalHari / (1000*60*60*24);
        long totalHarga = bedaHari * 90000;

        try {
            rentalMobil.setHari((int) bedaHari);
            rentalMobil.setPrice((int) totalHarga);
            this.rentalMobilService.saveRentalMobil(rentalMobil);
            return new ResponseEntity<RentalMobil> (rentalMobil, HttpStatus.OK);
        } catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message",e.getMessage());
            result.put("Data",null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("rentalmobil/{id}")
    public ResponseEntity<?> editRentalMobil(@RequestBody RentalMobil rentalMobil, @PathVariable("id") Long id){
        try {
            rentalMobil.setId(id);
            this.rentalMobilService.saveRentalMobil(rentalMobil);
            Map<String, Object> result = new HashMap<>();
            result.put("Message", "Edit Data Success");
            result.put("Status", "200");
            result.put("Data", rentalMobil);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message",e.getMessage());
            result.put("Data",null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("rentalmobil/{id}")
    public ResponseEntity<?> deleteRentalMobil(@PathVariable("id") Long id){
        try {
            RentalMobil rentalMobil = this.rentalMobilService.getRentalMobilById(id);
            if (rentalMobil != null){
                this.rentalMobilService.deleteRentalMobil(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found");
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("rentalmobil={keyword}")
    public ResponseEntity<List<RentalMobil>> searchRental(@PathVariable("keyword") String keyword)
    {
        try {
            List<RentalMobil> rentalMobil = this.rentalMobilService.searchRental(keyword);
            return new ResponseEntity<>(rentalMobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
