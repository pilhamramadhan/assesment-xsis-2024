package xa.assement.assement_Rental_Mobil.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.assement.assement_Rental_Mobil.models.JenisMobil;
import xa.assement.assement_Rental_Mobil.repositories.JenisMobilRepo;
import xa.assement.assement_Rental_Mobil.services.JenisMobilService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class JenisMobilRestController {

    @Autowired
    private JenisMobilService jenisMobilService ;

    @Autowired
    private JenisMobilRepo jenisMobilRepo;

    private Map<String, Object> result;

    @GetMapping("jenismobil")
    public ResponseEntity<List<JenisMobil>> getAllJenisMobil(){
        try {
            List<JenisMobil> jenisMobil = this.jenisMobilService.getAllJenisMobil();
            return new ResponseEntity<List<JenisMobil>>(jenisMobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("jenismobil/{id}")
    public ResponseEntity<?> getJenisMobilById(@PathVariable("id") Long id){
        try {
            JenisMobil jenisMobil = this.jenisMobilService.getJenisMobilById(id);
            return new ResponseEntity <>(jenisMobil, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("jenismobil/result")
    public ResponseEntity<List<JenisMobil>> searchJenis(@RequestParam("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<JenisMobil> jenisMobil = this.jenisMobilRepo.Search(keyword);
            return new ResponseEntity<>(jenisMobil, HttpStatus.OK);
        } else {
            List<JenisMobil> jenisMobil = this.jenisMobilRepo.findAll();
            return new ResponseEntity<>(jenisMobil, HttpStatus.OK);
        }
    }


//    @PostMapping("jenismobil")
//    public ResponseEntity<JenisMobil> saveJenisMobil(@RequestBody JenisMobil jenisMobil){
//        try {
//            this.jenisMobilService.saveJenisMobil(jenisMobil);
//            return new ResponseEntity<JenisMobil> (jenisMobil, HttpStatus.OK);
//        } catch (Exception e){
//            Map<String, String> result = new HashMap<>();
//            result.put("Message",e.getMessage());
//            result.put("Data",null);
//            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @PostMapping("jenismobil")
    public ResponseEntity<Object> saveJenisMobil(@RequestBody JenisMobil jenisMobil){
        try {
            Optional<JenisMobil> data = this.jenisMobilService.searchByJenis(jenisMobil.getJenis().trim());
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                this.jenisMobilService.saveJenisMobil(jenisMobil);
                result.put("status", "belum");
                result.put("data", jenisMobil);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        } catch (Exception e){
            result.put("pesan", e.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("jenismobil/{id}")
    public ResponseEntity<?> editJenisMobil (@RequestBody JenisMobil jenisMobil, @PathVariable("id") Long id){
        try {
            jenisMobil.setId(id);
            Optional<JenisMobil> idJenisMob = this.jenisMobilService.getIdJenisMobil(id);
            result = new HashMap<>();
            if (idJenisMob.isPresent()) {
                JenisMobil dataJenisMobil = idJenisMob.get();
                if (jenisMobil.getJenis().equals(dataJenisMobil.getJenis())) { // cek nama sama dengan awal
                    dataJenisMobil.setJenis(jenisMobil.getJenis());
                    this.jenisMobilService.saveJenisMobil(jenisMobil);
                    result.put("Pesan", "Berhasil");
                    result.put("data", jenisMobil);
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else {
                    Optional<JenisMobil> jenisMobSama = this.jenisMobilService.searchByJenis(jenisMobil.getJenis().trim());
                    if (jenisMobSama.isPresent()){ // jika nama ada yang sama
                        result.put("status", "ada");
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    } else { // jika nama tidak ada yang sama
                        dataJenisMobil.setJenis(jenisMobil.getJenis());
                        this.jenisMobilService.saveJenisMobil(jenisMobil);
                        result.put("Pesan", "Berhasil");
                        result.put("data", jenisMobil);
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    }
                }
            } else {
                result.put("Pesan", "ID tidak ditemukan.");
                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            result.put("pesan", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @PutMapping("jenismobil/{id}")
//    public ResponseEntity<?> editJenisMobil (@RequestBody JenisMobil jenisMobil, @PathVariable("id") Long id){
//        try {
//            jenisMobil.setId(id);
//            Optional<JenisMobil> jenisMob = this.jenisMobilService.searchByJenis(jenisMobil.getJenis().trim());
//            Optional<JenisMobil> idJenisMob = this.jenisMobilService.getIdJenisMobil(id);
//            result = new HashMap<>();
//            if (jenisMob.isEmpty() && idJenisMob.isPresent()){
//                result.put("status", "belum ada");
//                this.jenisMobilService.saveJenisMobil(jenisMobil);
//                result.put("Pesan", "Berhasil");
//                result.put("data", jenisMobil);
//                return new ResponseEntity<>(result, HttpStatus.OK);
//            } else if (jenisMob.isPresent() && idJenisMob.isPresent()){
//                result.put("status", "sudah ada");
//                return new ResponseEntity<>(result, HttpStatus.OK);
//            } else {
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//            }
//        } catch (Exception e){
//            result.put("pesan", e.getMessage());
//            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }


//    @PutMapping("jenismobil/{id}")
//    public ResponseEntity<?> editCustomer(@RequestBody JenisMobil jenisMobil, @PathVariable("id") Long id){
//        try {
//            jenisMobil.setId(id);
//            this.jenisMobilService.saveJenisMobil(jenisMobil);
//            Map<String, Object> result = new HashMap<>();
//            result.put("Message", "Edit Data Success");
//            result.put("Status", "200");
//            result.put("Data", jenisMobil);
//            return new ResponseEntity<>(result, HttpStatus.OK);
//        } catch (Exception e){
//            Map<String, String> result = new HashMap<>();
//            result.put("Message",e.getMessage());
//            result.put("Data",null);
//            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @DeleteMapping("jenismobil/{id}")
    public ResponseEntity<?> deleteJenisMobil(@PathVariable("id") Long id){
        try {
            JenisMobil jenisMobil = this.jenisMobilService.getJenisMobilById(id);
            if (jenisMobil != null){
                this.jenisMobilService.deleteJenisMobil(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found");
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


}
