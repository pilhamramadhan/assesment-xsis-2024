package xa.assement.assement_FrontEnd_Rental_Mobil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssementFrontEndRentalMobilApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssementFrontEndRentalMobilApplication.class, args);
	}

}
