package xa.assement.assement_FrontEnd_Rental_Mobil.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/mobil")
public class MobilController {
    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("mobil/mobil");
        return view;
    }

}
