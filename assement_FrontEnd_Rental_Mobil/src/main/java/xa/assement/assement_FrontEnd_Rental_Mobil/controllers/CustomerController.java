package xa.assement.assement_FrontEnd_Rental_Mobil.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @GetMapping
    public ModelAndView index(@RequestParam(defaultValue = "1")Integer halaman,
                              @RequestParam(defaultValue = "5")Integer ukuranHal){
        ModelAndView view = new ModelAndView("customer/customer");
        view.addObject("halaman", halaman);
        view.addObject("ukuranHal", ukuranHal);
        return view;
    }
}
