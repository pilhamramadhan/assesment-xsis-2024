const halaman = $("#halaman").val();
const ukuranHal = $("#ukuranHal").val();

getAllCustomer();

//    function getAllCustomer(){
//        $.ajax({
//            url:host + '/api/customer',
//            type:'get',
//            contentType:'application/json',
//            success: function(Data){
//                console.log(Data);
//                var str = '';
//                for(i = 0; i<Data.length; i++){
//                    str +=`
//                        <tr>
//                            <td>${i+1}</td>
//                            <td>${Data[i].nama}</td>
//                            <td>${Data[i].jenisKelamin}</td>
//                            <td>${Data[i].alamat}</td>
//                            <td><button class="btn btn-dark" onClick="formCustomer(${Data[i].id})"> Edit </button></td>
//                            <td><button class="btn btn-danger" onClick="deleteCustomer(${Data[i].id})"> Delete </button></td>
//                        </tr>
//                    `
//                }
//                $('#CustomerData').html(str)
//            }
//        })
//    }


    function getAllCustomer(){
        $.ajax({
        url:host + '/api/customer/mapped?halaman='+halaman+'&ukuranHal='+ukuranHal,
        type:'get',
        contentType:'application/json',
        success: function(Data){
            console.log(Data);
            var halSekarang = Data.halamanSekarang;
            var halTotal = Data.totalHalaman;

            var str = '';
            for(i = 0; i<Data.customerAPI.length; i++){
                str +=`
                    <tr>
                        <td>${i+1}</td>
                        <td>${Data.customerAPI[i].nama}</td>
                        <td>${Data.customerAPI[i].jenisKelamin}</td>
                        <td>${Data.customerAPI[i].alamat}</td>
                        <td><button class="btn btn-dark" onClick="formCustomer(${Data.customerAPI[i].id})"> Edit </button></td>
                        <td><button class="btn btn-danger" onClick="deleteCustomer(${Data.customerAPI[i].id})"> Delete </button></td>
                    </tr>
                `
            }
            $('#CustomerData').html(str)

            const url_page = "http://localhost:9091/customer?"
            $("#halamanNav").html(``);
            if(halSekarang != 0){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}halaman=${halSekarang-1}&ukuranHal=${ukuranHal}">Previous</a></li>
                `
                );
            }
            for(i = 0; i<Data.totalHalaman; i++){
                if(i == Data.halamanSekarang){
                    $("#halamanNav").append(
                    `
                        <li class="page-item disabled"><a class="page-link" href="${url_page}halaman=${i}&ukuranHal=${ukuranHal}">${(i+1)}</a></li>
                    `
                    );
                }else{
                    $("#halamanNav").append(
                    `
                        <li class="page-item"><a class="page-link" href="${url_page}halaman=${i}&ukuranHal=${ukuranHal}">${(i+1)}</a></li>
                    `
                    );
                }
            }
            if(halSekarang != (halTotal - 1) && halTotal > 0){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}halaman=${halaman+1}&ukuranHal=${ukuranHal}">Next</a></li>
                `
                );
            }
        }
        })
    }




function formCustomer(id){
    var str = '';
        if(id){
            $.ajax({
                url:host + '/api/customer/' + id,
                type:'get',
                contentType:'application/json',
                async:false,
                success: function(dataform){
                        str = `Nama : <input class="form-control" type="text" id="nama" value="${dataform.nama}">
                        <small class="errorField text-danger" type="text" id="errNama"></small>
                                    <form id="genderForm">
                                    Jenis Kelamin :
                                    <p><input type="radio" id="jenisKelaminLaki" name="jenisKelamin" value="Laki-laki" > Laki-laki</p>
                                    <p><input type="radio" id="jenisKelaminPerempuan" name="jenisKelamin" value="Perempuan"> Perempuan</p>
                                    <small class="errorField text-danger" type="text" id="errJenisKelamin"></small> <br>
                                    </form>
                        Alamat : <input class="form-control" type="text" id="alamat" value="${dataform.alamat}">
                        <small class="errorField text-danger" type="text" id="errAlamat"></small>
                        <hr>
                        <button class="btn btn-dark" onClick="editcustomer(${dataform.id})"> Simpan </button>
                    `;
                }
            })
        }
        else {
        str = `Nama : <input class="form-control" type="text" id="nama" >
        <small class="errorField text-danger" type="text" id="errNama"></small> <br>
            <form id="genderForm">
            Jenis Kelamin :
            <p><input type="radio" id="jenisKelaminLaki" name="jenisKelamin" value="Laki-laki" > Laki-laki</p>
            <p><input type="radio" id="jenisKelaminPerempuan" name="jenisKelamin" value="Perempuan"> Perempuan</p>
            <small class="errorField text-danger" type="text" id="errJenisKelamin"></small> <br>
            </form>
        Alamat : <input class="form-control" type="text" id="alamat">
        <small class="errorField text-danger" type="text" id="errAlamat"></small>
            <hr>
            <button class="btn btn-dark" onClick="simpancustomer()"> Simpan </button>
        `;
        }
         $('#mymodal').modal('show')
         $('.modal-title').html('customer')
         $('.modal-body').html(str);
    }

    function simpancustomer(){
        var nama = $("#nama").val().trim();
	    var alamat = $("#alamat").val().trim();
	    var form = document.getElementById('genderForm');
        var selectedGender = form.querySelector('input[name="jenisKelamin"]:checked');
        console.log(selectedGender);
        if (selectedGender) {
            var hasilJenisKelamin = selectedGender.value;
            console.log('Jenis Kelamin yang dipilih:', hasilJenisKelamin);
        }

	    var verif = false;

	    if(nama == ""){
	        $("#errNama").text("Tidak Boleh Kosong")
	        verif = true;
	    }
	    if(hasilJenisKelamin == null){
            $("#errJenisKelamin").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(alamat == ""){
            $("#errAlamat").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    const customer = {
	        nama: nama,
	        jenisKelamin: hasilJenisKelamin,
	        alamat: alamat
	    };
	    console.log(customer)

	    $.ajax({
            url : host + '/api/customer',
		    type : 'POST',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(customer),
		    success: function(result){
		    console.log(result)
		        if(result.status == "ada" ){
		            alert("data sudah ada")
		        } else {
		                $('#mymodal').modal('hide')
                        getAllCustomer();
		        }
		    }
	    })
    }


    function editcustomer(id){
        var nama = $("#nama").val().trim();
	    var form = document.getElementById('genderForm');
        var selectedGender = form.querySelector('input[name="jenisKelamin"]:checked');
        console.log(selectedGender);
         if (selectedGender) {
             var hasilJenisKelamin = selectedGender.value;
             console.log('Jenis Kelamin yang dipilih:', hasilJenisKelamin);
             // Di sini Anda bisa melakukan sesuatu dengan nilai jenis kelamin yang dipilih, misalnya mengirimkan ke server
         } else {
             console.log('Anda belum memilih jenis kelamin.');
             // Atau berikan pesan kesalahan kepada pengguna bahwa jenis kelamin harus dipilih
         }

	    var alamat = $("#alamat").val().trim();

	    var verif = false;

        if(nama == ""){
            $("#errNama").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(hasilJenisKelamin == null){
            $("#errJenisKelamin").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(alamat == ""){
            $("#errAlamat").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    const formCustomer = {
	        nama: nama,
	        jenisKelamin: hasilJenisKelamin,
	        alamat: alamat
	    };
	    console.log(formCustomer)

	    $.ajax({
            url : host + '/api/customer/' + id,
		    type : 'PUT',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(formCustomer),
		    success: function(result){
		        console.log(result)
                if(result.status == "ada" ){
                    alert("data sudah ada")
                } else {
                        $('#mymodal').modal('hide')
                        getAllCustomer();
                }
		    }
	    })
    }

    function deleteCustomer(id){
        var conf = window.confirm("Yakin Menghapus Data Ini ?");
        if(conf){
        $.ajax({
            url : host + '/api/customer/' + id,
		    type : 'DELETE',
		    contentType : "application/json",
		    success: function(result){
		        console.log(result)
		        $('#mymodal').modal('hide')
				getAllCustomer();
		    }
	    })
        }
    }

     $("#searchBtn").click(function(){
        var keyword = $("#search").val();
        console.log(keyword);
        $("#searchTable").html(
        	`
        	<thead>
                <th>NO</th>
                <th>Name</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Action</th>
            </thead>
                <tbody id="CustomerData">
                </tbody>
            `
        );

        $.ajax({
            url: host + '/api/customer=' + keyword,
            type: 'GET',
            contentType: 'application/json',
            success:function(searchData){
                var str = '';
                for(i = 0; i<searchData.length; i++){
                    str += `
                       <tr>
                            <td>${i+1}</td>
                            <td>${searchData[i].nama}</td>
                            <td>${searchData[i].jenisKelamin}</td>
                            <td>${searchData[i].alamat}</td>
                            <td><button class="btn btn-dark" onClick="formCustomer(${searchData[i].id})"> Edit </button></td>
                            <td><button class="btn btn-danger" onClick="deleteCustomer(${searchData[i].id})"> Delete </button></td>
                        </tr>
                    `;
                }
                $('#CustomerData').html(str)
            }
        })
    })