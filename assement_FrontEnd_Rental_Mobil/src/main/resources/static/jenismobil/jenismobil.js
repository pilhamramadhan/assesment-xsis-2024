getAllJenisMobil();

    function getAllJenisMobil(){
        $.ajax({
            url:host + '/api/jenismobil',
            type:'get',
            contentType:'application/json',
            success: function(Data){
                console.log(Data);
                var str = '';
                for(i = 0; i<Data.length; i++){
                    str +=`
                        <tr>
                            <td>${i+1}</td>
                            <td>${Data[i].jenis}</td>
                            <td>${Data[i].description}</td>
                            <td><button class="btn btn-dark" onClick="formJenis(${Data[i].id})"> Edit </button></td>
                            <td><button class="btn btn-danger" onClick="deleteJenis(${Data[i].id})"> Delete </button></td>
                        </tr>
                    `
                }
                $('#JenisMobilData').html(str)
            }
        })
    }

    function formJenis(id){
    var str = '';
        if(id){
            $.ajax({
                url:host + '/api/jenismobil/' + id,
                type:'get',
                contentType:'application/json',
                async:false,
                success: function(Data){
                        str = `
                        jenis mobil : <input class="form-control" type="text" id="jenis" value="${Data.jenis}">
                                     <small class="errorField text-danger" type="text" id="errJenis"></small> <br>
                        Description : <input class="form-control" type="text" id="description" value="${Data.description}">
                                     <small class="errorField text-danger" type="text" id="errDescription"></small> <br>
                        <hr>
                        <button class="btn btn-dark" onClick="editjenismobil(${Data.id})"> Simpan </button>
                    `;
                }
            })
        }
        else {
        str = `
                jenis : <input class="form-control" type="text" id="jenis" >
                        <small class="errorField text-danger" type="text" id="errJenis"></small> <br>
                Description : <input class="form-control" type="text" id="description" >
                        <small class="errorField text-danger" type="text" id="errDescription"></small> <br>
                <hr>
                <button class="btn btn-dark" onClick="simpanjenismobil()"> Simpan </button>
            `;
        }
         $('#mymodal').modal('show')
         $('.modal-title').html('jenis mobil')
         $('.modal-body').html(str);
    }

    function simpanjenismobil(){
        var jenis = $("#jenis").val().trim();
	    var description = $("#description").val().trim();

	    var verif = false;

        if(jenis == ""){
            $("#errJenis").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(description == ""){
            $("#errDescription").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    const jenisMobilIn = {
	        jenis: jenis,
	        description: description
	    };
	    console.log(jenisMobilIn)

	    $.ajax({
            url : host + '/api/jenismobil',
		    type : 'POST',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(jenisMobilIn),
		    success: function(result){
		        console.log(result)
		       if(result.status == "ada" ){
                   alert("data sudah ada")
               }else{
		        $('#mymodal').modal('hide')
				getAllJenisMobil();
               }
		    }
	    })
    }


    function editjenismobil(id){
        var jenis = $("#jenis").val().trim();
	    var description = $("#description").val().trim();

	    var verif = false;

        if(jenis == ""){
            $("#errJenis").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(description == ""){
            $("#errDescription").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    const formjenis = {
	        jenis: jenis,
	        description: description
	    };
	    console.log(formjenis)

	    $.ajax({
            url : host + '/api/jenismobil/' + id,
		    type : 'PUT',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(formjenis),
		    success: function(result){
		        console.log(result)
		        if(result.status == "ada" ){
                    alert("data sudah ada")
                }else{
                 $('#mymodal').modal('hide')
                	getAllJenisMobil();
                }

		    }
	    })
    }

    function deleteJenis(id){
        var conf = window.confirm("Yakin Menghapus Data Ini ?");
        if(conf){
        $.ajax({
            url : host + '/api/jenismobil/' + id,
		    type : 'DELETE',
		    contentType : "application/json",
		    success: function(result){
		        console.log(result)
		        $('#mymodal').modal('hide')
				getAllJenisMobil();
		    }
	    })
        }
    }


    function searchJenisMobil(keyword){
        if (keyword.length > 0){
            $.ajax({
                url: host + '/api/jenismobil/result?keyword=' + keyword,
                type: 'GET',
                contentType: 'application/json',
                success:function(searchData){
                    var str = '';
                    for(i = 0; i<searchData.length; i++){
                        str += `
                           <tr>
                                <td>${i+1}</td>
                                <td>${searchData[i].jenis}</td>
                                <td>${searchData[i].description}</td>
                                <td><button class="btn btn-dark" onClick="formJenis(${searchData[i].id})"> Edit </button></td>
                                <td><button class="btn btn-danger" onClick="deleteJenis(${searchData[i].id})"> Delete </button></td>
                            </tr>
                        `;
                    }
                    $('#JenisMobilData').html(str)
                }
            })
        } else {
            getAllJenisMobil();
        }
    }