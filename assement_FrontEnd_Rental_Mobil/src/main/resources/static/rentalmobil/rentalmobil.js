getAllRentalMobil();

        function getAllRentalMobil(){
            $.ajax({
                url:host + '/api/rentalmobil',
                type:'get',
                contentType:'application/json',
                success: function(Data){
                    console.log(Data);
                    var str = '';
                    for(i = 0; i<Data.length; i++){
                        str +=`
                            <tr>
                                <td>${i+1}</td>
                                <td>
                                <button class="btn btn-danger" onClick="deleteRentalMobil(${Data[i].id})">
                                <i class="bi bi-person-dash-fill"></i>
                                </button>
                                <button class="btn btn-primary" onClick="viewRentalMobil(${Data[i].id})">
                                <i class="bi bi-clipboard2"></i>
                                </button>
                                </td>
                                <td>${Data[i].customer.nama}</td>
                                <td>${Data[i].mobil.mobil}</td>
                                <td>${Data[i].hari} Hari</td>
                                <td>Rp ${Data[i].price}</td>
                            </tr>
                        `
                    }
                    $('#RentalMobilData').html(str)
                }
            })
        }

    function formRentalMobil(id){
    var str = ``;
    var customerRen;
        $.ajax({
            url:host + '/api/customer',
            type:'get',
            contentType:'application/json',
            async:false,
            success: function(Data){
                    customerRen = Data;
                    console.log(Data)
            }
        })

    var mobil;
        $.ajax({
            url:host + '/api/mobil',
            type:'get',
            contentType:'application/json',
            async:false,
            success: function(Data){
                    mobil = Data;
            }
        })

        if(id){
            $.ajax({
                url:host + '/api/rentalmobil/' + id,
                type:'get',
                contentType:'application/json',
                async:false,
                success: function(Data){
                        str = `Nama Customer : <select class="form-select" id="customerRen" >`;
                        console.log(customerRen);
                        for(i=0; i < customerRen.length; i++){
                          if(Data.customer.id === customerRen[i].id){
                            str += `<option value="${customerRen[i].id}" selected>${customerRen[i].nama}</option>`;
                          }else{
                            str += `<option value="${customerRen[i].id}">${customerRen[i].nama}</option>`;
                          }
                        }
                        str += `</select>`

                        str += `Pilihan Mobil : <select class="form-select" id="mobil" >`;
                        console.log(mobil);
                        for(i=0; i < mobil.length; i++){
                          if(Data.mobil.id === mobil[i].id){
                            str += `<option value="${mobil[i].id}" selected>${mobil[i].mobil}</option>`;
                          }else{
                            str += `<option value="${mobil[i].id}">${mobil[i].mobil}</option>`;
                          }
                        }
                        str += `</select>

                        Tanggal sewa : <input class="form-control" type="Date" id="tglsewa" value="${Data.tglSewa}">
                        Tanggal Balik : <input class="form-control" type="Date" id="tglbaliksewa" value="${Data.tglBalikSewa}">
                        <hr>
                        <button class="btn btn-primary" onclick="editRentalMobil(${Data.id})">simpan</button>`
                }
            })
        }
        else {
                str = `Nama Customer : <select class="form-select" id="customerRen">
                                        <small class="errorField text-danger" type="text" id="errCusRen"></small> <br>`;
                console.log(customerRen);
                for(i=0; i < customerRen.length; i++){
                  str += `<option value="${customerRen[i].id}">${customerRen[i].nama}</option>`;
                }
                str += `</select>`;

                str += `Pilihan Mobil : <select class="form-select" id="mobil"
                                        <small class="errorField text-danger" type="text" id="errMobil"></small> <br>>`;
                console.log(mobil);
                for(i=0; i < mobil.length; i++){
                  str += `<option value="${mobil[i].id}">${mobil[i].mobil}</option>`;
                }
                str += `</select>

                Tanggal sewa : <input class="form-control" type="date" id="tglsewa">
                                        <small class="errorField text-danger" type="text" id="errtglsewa"></small> <br>
                Tanggal Balik : <input class="form-control" type="date" id="tglbaliksewa">
                                        <small class="errorField text-danger" type="text" id="errtglbaliksewa"></small> <br>

                <hr>
                <button class="btn btn-primary" onclick="simpanRentalMobil()">simpan</button>`
            }
         $('#mymodal').modal('show')
         $('.modal-title').html('Rental Mobil')
         $('.modal-body').html(str);
    }

    function simpanRentalMobil(){
        var customerRen = $("#customerRen").val();
        var mobil = $("#mobil").val();
	    var tglsewa = $("#tglsewa").val();
	    var tglbaliksewa = $("#tglbaliksewa").val();
	    var price = $("#price").val();

	    var verif = false;
	    if(customerRen == ""){
            $("#errJenis").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(mobil == ""){
            $("#errDescription").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(tglbaliksewa < tglsewa){
            $("#errtglsewa").text("tanggal sewa tidak boleh hari kemarin")
            $("#errtglbaliksewa").text("tanggal sewa tidak boleh hari kemarin")
            verif = true;
        }
        if (verif){
            return
        }

	    const MobilIn = {
	        customerId: customerRen,
	        mobilId: mobil,
	        tglSewa: tglsewa,
	        tglBalikSewa: tglbaliksewa,
	        price: price
	    };
	    console.log(MobilIn)

	    $.ajax({
            url : host + '/api/rentalmobil',
		    type : 'POST',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(MobilIn),
		    success: function(result){
		        console.log(result)
		        $('#mymodal').modal('hide')
				getAllRentalMobil();
		    }
	    })
    }


    function editRentalMobil(id){
        var customerRen = $("#customerRen").val();
        var mobil = $("#mobil").val();
	    var tglsewa = $("#tglsewa").val();
	    var tglbaliksewa = $("#tglbaliksewa").val();
	    var price = $("#price").val();

	    const formmobil = {
	        customerId: customerRen,
	        mobilId: mobil,
	        tglSewa: tglsewa,
	        tglBalikSewa: tglbaliksewa,
	        price: price
	    };
	    console.log(formmobil)

	    $.ajax({
            url : host + '/api/rentalmobil/' + id,
		    type : 'PUT',
		    datatype : 'json',
            contentType : "application/json",
		    data : JSON.stringify(formmobil),
		    success: function(result){
		        console.log(result)
		        $('#mymodal').modal('hide')
				getAllRentalMobil();
		    }
	    })
    }

    function deleteRentalMobil(id){
        var conf = window.confirm("Yakin Menghapus Data Ini ?");
        if(conf){
        $.ajax({
            url : host + '/api/rentalmobil/' + id,
		    type : 'DELETE',
		    contentType : "application/json",
		    success: function(result){
		        console.log(result)
				getAllRentalMobil();
		    }
	    })
        }
    }

    function viewRentalMobil(id){
        $.ajax({
            url : host + '/api/rentalmobil/' + id,
		    type : 'GET',
		    contentType : "application/json",
		    success: function(Data){
		        console.log(Data)
		        var str = '';
                    str += `
                    <div
                          <p>Nama Customer : ${Data.customer.nama}</p>
                          <p>Nama Mobil : ${Data.mobil.mobil}</p>
                          <p>Berapa hari : ${Data.hari} Hari</p>
                          <p>Harga : Rp ${Data.price}</p>
                    `;
		        $('#mymodal').modal('show')
		        $(".modal-title").text("Detail Sewa");
		        $(".modal-body").html(str);
		    }
	    })
    }

    $("#searchBtn").click(function(){
        var keyword = $("#search").val();
        console.log(keyword);
        $("#searchTable").html(
        	`
        	<thead>
    <th>NO</th>
    <th>Nama Customer</th>
    <th>Nama Mobil</th>
    <th>Tgl sewa</th>
    <th>Tgl Balik</th>
    <th>price</th>
    <th>Action</th>
    </thead>
    <tbody id="RentalMobilData">

    </tbody>
            `
        );

        $.ajax({
            url: host + '/api/rentalmobil=' + keyword,
            type: 'GET',
            contentType: 'application/json',
            success:function(Data){
                var str = '';
                for(i = 0; i<Data.length; i++){
                    str += `
                          <tr>
                              <td>${i+1}</td>
                              <td>
                              <button class="btn btn-danger" onClick="deleteRentalMobil(${Data[i].id})">
                              <i class="bi bi-person-dash-fill"></i>
                              </button>
                              <button class="btn btn-danger" onClick="viewRentalMobil(${Data[i].id})">
                              <i class="bi bi-clipboard2"></i>
                              </button>
                              </td>
                              <td>${Data[i].customer.nama}</td>
                              <td>${Data[i].mobil.mobil}</td>
                              <td>${Data[i].hari} Hari</td>
                              <td>Rp ${Data[i].price}</td>
                          </tr>
                    `;
                }
                $('#RentalMobilData').html(str)
            }
        })
    })