 getAllMobil();

    function getAllMobil(){
        $.ajax({
            url:host + '/api/mobil',
            type:'get',
            contentType:'application/json',
            success: function(Data){
                console.log(Data);
                var str = '';
                for(i = 0; i<Data.length; i++){
                    str +=`
                        <tr>
                            <td>${i+1}</td>
                            <td>${Data[i].mobil}</td>
                            <td>${Data[i].jenisMobil.jenis}</td>
                            <td><img src="${host}${Data[i].image_path}"></td>
                            <td>${Data[i].bahanBakar}</td>
                            <td><button class="btn btn-dark" onClick="formMobil(${Data[i].id})"> Edit </button></td>
                            <td><button class="btn btn-danger" onClick="deleteMobil(${Data[i].id})"> Delete </button></td>
                        </tr>
                    `
                }
                $('#MobilData').html(str)
            }
        })
    }



    function formMobil(id){
    var str = ``;
    var jenismobil;
        $.ajax({
            url:host + '/api/jenismobil',
            type:'get',
            contentType:'application/json',
            async:false,
            success: function(Data){
                    jenismobil = Data;
            }
        })

        if(id){
            $.ajax({
                url:host + '/api/mobil/' + id,
                type:'get',
                contentType:'application/json',
                async:false,
                success: function(Data){
                     str = `Nama Mobil : <input class="form-control" type="text" id="namamobil" value="${Data.mobil}">
                                      <small class="errorField text-danger" type="text" id="errMobil"></small> <br> `;
                        str += `Jenis Mobil : <select class="form-select" id="jenismobil" >
                                             <small class="errorField text-danger" type="text" id="errJenisMobil"></small> <br> `;
                        console.log(jenismobil);
                        for(i=0; i < jenismobil.length; i++){
                          if(Data.jenisMobil.id === jenismobil[i].id){
                            str += `<option value="${jenismobil[i].id}" selected>${jenismobil[i].jenis}</option>`;
                          }else{
                            str += `<option value="${jenismobil[i].id}">${jenismobil[i].jenis}</option>`;
                          }
                        }
                        str += `</select>
                        Bahan Bakar : <input class="form-control" type="text" id="bahanbakar" value="${Data.bahanBakar}">
                                     <small class="errorField text-danger" type="text" id="errBahanBakar"></small> <br>
                        Image : <input type="file" id="imagePath" class="form-control" value="${Data.image_path}">
                                 <small class="errorField text-danger" type="text" id="errImage"></small> <br>
                        <hr>
                        <button class="btn btn-primary" onclick="editMobil(${Data.id})">simpan</button>`
                }
            })
        }
        else {
        str = `Nama Mobil : <input class="form-control" type="text" id="namamobil">
                         <small class="errorField text-danger" type="text" id="errMobil"></small> <br>`;
                str += `Jenis Mobil : <select class="form-select" id="jenismobil">
                         <small class="errorField text-danger" type="text" id="errJenisMobil"></small> <br>`;
                console.log(jenismobil);
                for(i=0; i < jenismobil.length; i++){
                  str += `<option value="${jenismobil[i].id}" selected="selected">${jenismobil[i].jenis}</option>`;
                }
                str += `</select>
                Bahan Bakar : <input class="form-control" type="text" id="bahanbakar">
                        <small class="errorField text-danger" type="text" id="errBahanBakar"></small> <br>
                Image : <input type="file" id="imagePath" class="form-control">
                     <small class="errorField text-danger" type="text" id="errImage"></small> <br>
                <hr>
                <button class="btn btn-primary" onclick="simpanmobil()">simpan</button>`
            }
         $('#mymodal').modal('show')
         $('.modal-title').html('Mobil')
         $('.modal-body').html(str);
    }

    function simpanmobil(){
        var namamobil = $("#namamobil").val().trim();
        var jenismobil = $("#jenismobil").val().trim();
	    var bahanbakar = $("#bahanbakar").val().trim();

	    var fileInput = document.getElementById('imagePath');
        var file = fileInput.files[0];

        var verif = false;
        if(namamobil == ""){
            $("#errMobil").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(jenismobil == ""){
            $("#errJenisMobil").text("Tidak Boleh Kosong")
            verif = true;
        }if(bahanbakar == ""){
            $("#errBahanBakar").text("Tidak Boleh Kosong")
            verif = true;
        }if(file == null){
            $("#errImage").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    var formData = new FormData();
        formData.append('file', file);
        formData.append('mobil', JSON.stringify({
            mobil: namamobil,
            jenisMobilId: jenismobil,
            bahanBakar: bahanbakar
        }));

        $.ajax({
            url: host + '/api/mobil/addImage',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                console.log("Berhasil mengunggah file:", result);
                $('#mymodal').modal('hide');
                getAllMobil();
            }
        });
    }


    function editMobil(id){
        var namamobil = $("#namamobil").val().trim();
        var jenismobil = $("#jenismobil").val().trim();
	    var bahanbakar = $("#bahanbakar").val().trim();
        var fileInput = document.getElementById('imagePath');
        var file = fileInput.files[0];
	    var formData = new FormData();
        formData.append('file', file);
        formData.append('id', id);

        var verif = false;

        if(namamobil == ""){
            $("#errMobil").text("Tidak Boleh Kosong")
            verif = true;
        }
        if(jenismobil == ""){
            $("#errJenisMobil").text("Tidak Boleh Kosong")
            verif = true;
        }if(bahanbakar == ""){
            $("#errBahanBakar").text("Tidak Boleh Kosong")
            verif = true;
        }if(file == null){
            $("#errImage").text("Tidak Boleh Kosong")
            verif = true;
        }
        if (verif){
            return
        }

	    const formmobil = {
	        mobil: namamobil,
	        jenisMobilId: jenismobil,
	        bahanBakar: bahanbakar
	    };

	    console.log(formmobil)
	    console.log(file)
	    console.log(formData)

	    $.ajax({
            url : host + '/api/mobil/' + id,
		    type : 'PUT',
		    datatype : 'json',
            contentType : "application/json",
            async : false,
		    data : JSON.stringify(formmobil),
		    success: function(result){
		        console.log(result)
		        $.ajax({
		            url : host + '/api/mobil/addImage',
                    type : 'PUT',
                    data : formData,
                    contentType: false, // Tetapkan contentType ke false untuk mencegah jQuery menambahkan header "Content-Type"
                    processData: false, // Tetapkan processData ke false untuk mencegah jQuery memproses FormData secara otomatis
                    success: function(result){
                        $('#mymodal').modal('hide')
                        getAllMobil();
                    }
		        })
		    }
	    })
    }

    function deleteMobil(id){
        var conf = window.confirm("Yakin Menghapus Data Ini ?");
        if(conf){
        $.ajax({
            url : host + '/api/mobil/' + id,
		    type : 'DELETE',
		    contentType : "application/json",
		    success: function(result){
		        console.log(result)
		        $('#mymodal').modal('hide')
				getAllMobil();
		    }
	    })
        }
    }

    $("#searchBtn").click(function(){
        var keyword = $("#search").val();
        console.log(keyword);
        $("#searchTable").html(
        	`
        	 <thead>
    <th>NO</th>
    <th>Nama Mobil</th>
    <th>Jenis Mobil</th>
    <th>Bahan Bakar</th>
    <th>Action</th>
    </thead>
    <tbody id="MobilData">

    </tbody>
            `
        );

        $.ajax({
            url: host + '/api/mobil=' + keyword,
            type: 'GET',
            contentType: 'application/json',
            success:function(Data){
                var str = '';
                for(i = 0; i<Data.length; i++){
                    str += `
                          <tr>
                            <td>${i+1}</td>
                            <td>${Data[i].mobil}</td>
                            <td>${Data[i].jenisMobil.jenis}</td>
                            <td>${Data[i].bahanBakar}</td>
                            <td><button class="btn btn-dark" onClick="formMobil(${Data[i].id})"> Edit </button></td>
                            <td><button class="btn btn-danger" onClick="deleteMobil(${Data[i].id})"> Delete </button></td>
                        </tr>
                    `;
                }
                $('#MobilData').html(str)
            }
        })
    })

