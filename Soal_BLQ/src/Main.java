import java.util.Calendar;
import java.util.Locale;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
class result {
    public static String findDay(int month, int day, int year) {
        Calendar tanggal = Calendar.getInstance();
        tanggal.set(Calendar.MONTH, month-1);
        tanggal.set(Calendar.DAY_OF_MONTH, day);
        tanggal.set(Calendar.YEAR, year);
        String dayString = tanggal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US).toUpperCase();
        return dayString;
    }
}

public class Main {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        String name = "ilham ";
        System.out.printf("Hello and welcome!" );

        // Press Shift+F10 or click the green arrow button in the gutter to run the code.
        for (int i = 1; i <= 5; i++) {

            // Press Shift+F9 to start debugging your code. We have set one breakpoint
            // for you, but you can always add more by pressing Ctrl+F8.
            System.out.println("i = " + i);
        }

    }
}