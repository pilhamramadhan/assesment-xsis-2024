package SOAL_BLQ;

import java.util.Scanner;

public class Soal_5_BilanganFibonaci {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Nilai n = ");
        int n = input.nextInt();

        int helper1 = 0;
        int helper2 = 1;

        int[] hasil = new int[n];
        for (int i = 0; i < n; i++) {
            if (i == 0){
                hasil[i] = helper2;
            } else {
                hasil[i] = helper1 + helper2;
                helper1 = helper2;
                helper2 = hasil[i];
            }
        }

        System.out.println("Bilangan Fibonaci :");
        for (int i = 0; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");
        }
    }
}
