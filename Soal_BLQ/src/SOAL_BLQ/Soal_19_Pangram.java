package SOAL_BLQ;

import java.util.Scanner;

public class Soal_19_Pangram {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukan Kalimat = ");
        String kalimat = input.nextLine();
        String alfabet = "qwertyuiopasdfghjklzxcvbnm";
        int helper = 0;

        for (int i = 0; i < alfabet.length(); i++) {
            if (kalimat.toLowerCase().contains(alfabet.substring(i, i+1))){
                helper++;
            }
        }

        if (helper == 26){
            System.out.println("kalimat tersebut adalah pangram");
        } else {
            System.out.println("kalimat tersebut bukan pangram");
        }

    }
}
