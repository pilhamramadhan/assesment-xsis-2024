package SOAL_BLQ;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal_3_Parkir {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Contoh Input Tanggal : 28 Januari 2020 | 07:30:34");
        System.out.println("Masukkan Tanggal Masuk : ");
        String masuk = input.nextLine();
        System.out.println("Masukkan Tanggal Keluar : ");
        String keluar = input.nextLine();

        Date masukDate = null;
        Date keluarDate = null;

        DateFormat df = new SimpleDateFormat("dd MMMM yyyy | HH:mm:ss", new Locale("ID"));
        try {
            masukDate = df.parse(masuk);
        } catch (ParseException e) {
            System.out.println("Waktu Masuk yang Diinputkan Salah");
        }

        try {
            keluarDate = df.parse(keluar);
        } catch (ParseException e) {
            System.out.println("Waktu Keluar yang Diinputkan Salah");
        }
        long bedaWaktu = keluarDate.getTime() - masukDate.getTime();

        long jamBeda = bedaWaktu / (1000 * 60 * 60);
        System.out.println("Lama Anda Parkir : " + jamBeda + " Jam");

        int hargaParkir = 0;
        int berapaHari = 0;

        if (jamBeda <= 8) {
            hargaParkir = (int) jamBeda * 1000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        } else if (jamBeda > 8 && jamBeda <= 24) {
            hargaParkir = 8000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        } else {
            berapaHari = (int) jamBeda / 24;
            hargaParkir = (int) ((berapaHari * 15000) + ((jamBeda - (berapaHari * 24)) * 1000));
            System.out.println("Tarif Parkir : " + hargaParkir);
        }

    }
}