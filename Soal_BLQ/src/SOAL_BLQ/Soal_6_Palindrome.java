package SOAL_BLQ;

import java.util.Scanner;

public class Soal_6_Palindrome {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("kata palindrom adalah kata yang jika dibalik tetap sama");
        System.out.println("Masukan Kata Palindrom = ");
        String kalimat = input.nextLine();

        char[] kalimatAsli = kalimat.toCharArray();
        char[] kalimatBalik = new char[kalimatAsli.length];

        int helper = 1;

        for (int i = 0; i < kalimatAsli.length; i++) {
            kalimatBalik[i] = kalimatAsli[kalimatAsli.length - helper];
            helper++;
        }
        System.out.print("apakah kata ini palindrome = ");
        System.out.println( kalimatBalik);

        boolean apakahSama = true;
        for (int i = 0; i < kalimatAsli.length; i++) {
            if (kalimatBalik[i] == kalimatAsli[i]){
                apakahSama = true;
            } else {
                apakahSama = false;
            }
        }

        if (apakahSama == true){
            System.out.println("Kata Tersebut Palindrome");
        } else {
            System.out.println("Kata Tersebut Bukan Palindrome");
        }

    }
}
