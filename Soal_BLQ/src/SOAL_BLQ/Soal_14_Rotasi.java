package SOAL_BLQ;

import java.util.Scanner;

public class Soal_14_Rotasi {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan deret angka = ");
        String deretAngka = input.nextLine();

        String[] angkaSplit = deretAngka.split(" ");
        int[] angkaArray = new int[angkaSplit.length];
        for (int i = 0; i < angkaSplit.length; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        System.out.print("berapa kali Rotasi dalam bentuk N = ");
        int rotasi = input.nextInt();
        
        int helper;
        for (int i = 0; i < rotasi; i++) {
            helper = angkaArray[0];
            for (int j = 0; j < angkaArray.length; j++) {
                if (j == angkaArray.length - 1){
                    angkaArray[j] = helper;
                } else {
                    angkaArray[j] = angkaArray[j + 1];
                }
            }
        }

        boolean sama = true ;
        System.out.println("hasil dari Rotasi sejumlah  N = "+ rotasi);
        for (int i = 0; i < angkaSplit.length; i++) {
            System.out.print(angkaArray[i] + " ");
            if(angkaArray[i] == Integer.parseInt(angkaSplit[i])){
                sama = true;
            }

        }
            if(sama == true){
                System.out.println("rotasi sama seperti bentuk awal");
            } else {
                System.out.println("");
            }

    }
}
