package SOAL_BLQ;

import java.util.Arrays;
import java.util.Scanner;

public class Soal_8_NilaiTerbesarNilaiTerkecil {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan deret angka = ");
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        while (angkaSplit.length <= 4){
            System.out.println("deret angka harus minimal 4");
            System.out.println("masukan deret angka = ");
            angka = input.nextLine();

            angkaSplit = angka.split(" ");
        }

        int[] angkaArray = new int[angkaSplit.length];
        for (int i = 0; i < angkaSplit.length; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }


        int nilaiMax = 0;
        int nilaiMin = 0;

        int helper = 0;
        for (int i = 0; i < angkaArray.length ; i++) {
            for (int j = i + 1; j < angkaArray.length; j++) {
                if ( angkaArray[i] < angkaArray[j]){
                    helper = angkaArray[i];
                    angkaArray[i] = angkaArray[j];
                    angkaArray[j] = helper;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            nilaiMax += angkaArray[i];
        }

        Arrays.sort(angkaArray);
        for (int i = 0; i < 4; i++) {
            nilaiMin += angkaArray[i];
        }

        System.out.println("nilai maksimal = " + nilaiMax);
        System.out.println("nilai minimal = " + nilaiMin);
    }
}
