package SOAL_BLQ;

import java.util.Scanner;

public class Soal_15_KonversiFormatJam {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Silahkan Masukan jam beserta format nya PM atau AM = ");
        String masukanJam = input.nextLine();

        String subjam = masukanJam.substring(0,2);
        int jamInt = Integer.parseInt(subjam);
        String konversi = Integer.toString(jamInt);

        if (masukanJam.contains("am") || masukanJam.contains("AM")){
            if (jamInt >= 12){
                jamInt -= 12;
                masukanJam = masukanJam.replace(subjam, konversi);
                System.out.println("Dalam Bentuk Format 24 Jam :");
                System.out.println("0"+jamInt+":"+masukanJam.substring(2,4)+":"+masukanJam.substring(5,8));
            } else if (jamInt <= 12) {
                masukanJam = masukanJam.replace(subjam, konversi);
                System.out.println("Dalam Bentuk Format 24 Jam :");
                System.out.println(jamInt+":"+masukanJam.substring(2,4)+":"+masukanJam.substring(5,8));
            }
        }
        if (masukanJam.contains("pm") || masukanJam.contains("PM")){
            if (jamInt <= 12){
                jamInt += 12;
                masukanJam = masukanJam.replace(subjam, konversi);
                System.out.println("Dalam Bentuk Format 24 Jam :");
                System.out.println(jamInt+":"+masukanJam.substring(2,4)+":"+masukanJam.substring(5,8));
            } else if (jamInt >= 12) {
                masukanJam = masukanJam.replace(subjam, konversi);
                System.out.println("Dalam Bentuk Format 24 Jam :");
                System.out.println(jamInt+":"+masukanJam.substring(2,4)+":"+masukanJam.substring(5,8));
            }
        }
    }
}
