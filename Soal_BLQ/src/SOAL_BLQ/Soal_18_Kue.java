package SOAL_BLQ;

import java.util.Scanner;

public class Soal_18_Kue {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int jamOlahraga = 18, bedaJam, air;
        double lamaOlahraga = 0;
        int[] jamMakan = {9, 13, 15, 17};
        int[] kalori = {30, 20, 50, 80};

        for (int i = 0; i < jamMakan.length; i++) {
            bedaJam = jamOlahraga - jamMakan[i];
            lamaOlahraga = lamaOlahraga + (0.1 * kalori[i] * bedaJam);
            jamOlahraga = 18;
        }

        air = (int) (lamaOlahraga * 200) + 500;
        System.out.println(air + " cc air");

    }
}
