package SOAL_BLQ;

import java.util.Scanner;

public class Soal_4_BilanganPrima {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Nilai n = ");
        int n = input.nextInt();

        int helper = 1;
        boolean flag = true;

        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            while (flag){
                if (helper == 1){
                    helper++;
                    flag = false;
                } else if (helper == 2) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper == 3) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper % 2 != 0 && helper % 3 != 0) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else {
                    helper++;
                }
            }
            flag = true;
        }
        System.out.println("Bilangan Prima : ");
        for (int i = 1; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");
        }
    }
}
