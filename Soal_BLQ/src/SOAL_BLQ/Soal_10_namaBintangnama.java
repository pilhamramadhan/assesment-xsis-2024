package SOAL_BLQ;

import java.util.Scanner;

public class Soal_10_namaBintangnama {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("contoh Input  : Susilo Bambang Yudhoyono");
        System.out.println("    Output : S***o B***g Y***o");
        System.out.print("Masukkan Data : ");

        String text = input.nextLine();
        String[] textStringArray = text.split(" ");

        System.out.print("Output : ");
        for (int i = 0; i < textStringArray.length; i++) {
            char[] textCharArray = textStringArray[i].toCharArray();
            for (int j = 0; j < textCharArray.length; j++) {
                if (j == 0){
                    System.out.print(textCharArray[j] + "***");
                }
                else if (j == textCharArray.length - 1){
                    System.out.print(textCharArray[j] + " ");
                }
            }
        }
    }
}
