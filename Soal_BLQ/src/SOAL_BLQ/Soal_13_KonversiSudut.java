package SOAL_BLQ;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Soal_13_KonversiSudut {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("=====> Menghitung Derajat Jarum Jam <=====");
        System.out.println("Format input : 03:00 tidak lebih dari jam 12.00");
        System.out.println("Masukkan jam : ");
        String inputJam = input.nextLine();
        String[] splitInput = inputJam.split(":");
        int jam = Integer.parseInt(splitInput[0]);
        int menit = Integer.parseInt(splitInput[1]);
        int jarumMenit = (menit/60) * 12;
        int nilaiSudutPerJam = 30;

        int output = 0;
        if (menit > 0){
            jam += (menit / 60);
        }
        if (jam >= 12){
            jam -= 12;
        }

        if (jam > jarumMenit){
            output = (jam - jarumMenit) * nilaiSudutPerJam;
        } else if (jam < jarumMenit) {
            output = (jarumMenit - jam) * nilaiSudutPerJam;
        }

        if (output > 180){
            output = 360 - output;
        }

        System.out.println("Output = " + output + " Derajat");
    }
}
